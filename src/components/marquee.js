import React, { useState, useEffect } from 'react';
import { NoticeBar } from '@ant-design/react-native';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import { StyleSheet, View, Text } from 'react-native';
import { useFetchApi } from '../module/fetchApi';
import { uuid } from '../module/getUUID';

export const Marquee = () => {

  const { data, isLoading } = useFetchApi(`https://www.ktic.com.my/lookHere/api/marquee.php?security_code=36BBA69CBEA56FBFE09E17B31C26D57B&uid=${uuid}`)

  // console.log('data---Marquee', data)

  return (
    <View style={styles.marqueeContainer}>
      <NoticeBar
        marqueeProps={{ loop: true, fps: 150, style: { fontSize: RFPercentage(2.6), color: 'yellow', } }}
        style={styles.noticeBar}
        icon={null}
      // icon={false}
      // onPress={() => alert('click')}
      >
        {
          isLoading ? ' Is Loadding....' : data[0].marq_content
        }
      </NoticeBar>
    </View>
  )
}

const styles = StyleSheet.create({
  marqueeContainer: { position: 'absolute', bottom: 0, width: '100%', height: 50, },
  noticeBar: { height: 50, backgroundColor: '#000', color: 'yellow', },


});