import React, { useState, useEffect } from 'react';
import { NoticeBar } from '@ant-design/react-native';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import { StyleSheet, View, Text, Image, ActivityIndicator, Animated, Easing, } from 'react-native';
import { useFetchApi } from '../module/fetchApi';
import LottieView from 'lottie-react-native';
import AsyncStorage from '@react-native-community/async-storage';
import logo from '../../assets/logo/logo.jpg';
import cloudys from '../../assets/lottie/cloudy.json';
import clears from '../../assets/lottie/clear.json';
import mists from '../../assets/lottie/mist.json';
import rains from '../../assets/lottie/rain.json';
import snows from '../../assets/lottie/snow.json';
import storms from '../../assets/lottie/storm.json';
import windys from '../../assets/lottie/windy.json';



export const Information = ({ getUUID }) => {


  const [currentTime, setCurrentTime] = useState('');
  const [currentDate, setCurrentDate] = useState('');
  const [progress, setProgress] = useState(new Animated.Value(0))
  const [temperature, setTemperature] = useState('');
  const [weatherCondition, setWeatherCondition] = useState('');
  const [loadingWeather, setLoadingWeather] = useState(true);
  const [API_KEY, setAPI_KEY] = useState('187229a882ab9bd11b9adf4079d313d5')
  const [weatherData, setWeatherData] = useState([]);
  const [err, setErr] = useState('');
  const [location, setLocation] = useState([])


  const { data, isLoading } = useFetchApi('https://www.ktic.com.my/lookHere/api/logo.php?security_code=36BBA69CBEA56FBFE09E17B31C26D57B');
  console.log('data------Information', data);

  async function fetchUrl() {
    const response = await fetch(`https://www.ktic.com.my/lookHere/api/media_list_landscape.php?security_code=36BBA69CBEA56FBFE09E17B31C26D57B&uid=${getUUID}`);
    const json = await response.json();
    setLocation(json[0]);
  }

  async function fetchWeather() {
    const response = await fetch(`https://api.openweathermap.org/data/2.5/weather?lat=${location.dev_latitude}&lon=${location.dev_longitude}&APPID=${API_KEY}&units=metric`);
    const json = await response.json();
    setWeatherData(json);
    setLoadingWeather(false);
  }

  useEffect(() => {
    fetchUrl();
    fetchWeather();
  }, [])

  console.log('location------------', location.dev_latitude)

  console.log('weatherData------------', weatherData)




  //////////////////////////////////////////// runAnimation ////////////////////////////////////////////
  // const runAnimation = () => {
  //   Animated.timing(progress, {
  //     toValue: 1,
  //     duration: 3000,
  //     easing: Easing.linear,
  //   }).start(() => {
  //     runAnimation();
  //   });
  // }

  //////////////////////////////////////////// setAnim ////////////////////////////////////////////
  // const setAnim = anim => {
  //   this.anim = anim;
  // };


  return (
    <View>
      {/********************************************* ADVERTISE **********************************************/}
      <View style={{ height: '30%' }}>
        <View style={{ backgroundColor: '#000', width: '100%', padding: 5, }}>
          <Text style={{ color: '#fff', fontSize: RFPercentage(2), fontWeight: 'bold' }}>ADVERTISE</Text>
        </View>
        <View style={{ marginTop: 0, paddingLeft: 5 }}>
          <Text style={{ color: '#000', fontSize: RFPercentage(2), fontWeight: 'bold' }}>HERE</Text>
        </View>
        {
          isLoading ?
            <View style={styles.indicatorWarp}>
              <ActivityIndicator size="large" color="#de2d30" />
            </View> :
            <View style={{ width: '100%', height: '50%', paddingRight: 2, marginTop: 5 }} >
              <Image source={{ uri: data[0].logo }} style={{ width: '100%', height: '100%', resizeMode: 'contain', }} />
            </View>
        }
      </View>
      {/********************************************* Logo **********************************************/}
      <View style={{ width: '100%', height: '30%', }}>
        <Image source={logo} style={{ width: '100%', height: '100%', }} />
      </View>
      {/********************************************* Weather **********************************************/}
      <View style={{ width: '100%', backgroundColor: '#fff', bottom: 0, position: 'absolute', height: '40%', }} >
        <View style={{ width: '95%', }}>
          <View style={{ width: '100%', paddingTop: 5, paddingBottom: 5, paddingLeft: 5 }}>
            <Text style={{ textAlign: 'right', fontSize: RFPercentage(1.8), }}>{currentTime}</Text>
            <Text style={{ textAlign: 'right', fontSize: RFPercentage(1.8), }}>{currentDate}</Text>
          </View>
          {
            loadingWeather ?
              <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 10 }}>
                <ActivityIndicator size="large" color="#ccc" />
              </View>
              :
              <View style={{ width: '100%', }}>
                <View style={{ width: '100%', paddingLeft: 5, }}>
                  {/* <LottieView
                    ref={this.setAnim}
                    source={
                      weatherCondition === 'Clear' ? clears :
                        weatherCondition === 'Rain' ? rains :
                          weatherCondition === 'Thunderstorm' ? storms :
                            weatherCondition === 'Snow' ? snows :
                              weatherCondition === 'Mist' ? mists :
                                weatherCondition === 'Clouds' ? cloudys :
                                  windys
                    }
                    progress={progress}
                    loop={loop}
                    autoPlay={progress}
                    style={{ justifyContent: 'center', alignItems: 'center', width: '80%', }}
                    resizeMode="cover"
                  /> */}
                  {/* <Image source={{ uri: `http://openweathermap.org/img/w/${weatherIcon}.png` }}
                          style={{ width: '100%', height: '100%', textAlign: 'center', }} />
                        <Text>{weatherCondition}</Text> */}
                </View>
                <View style={{ width: '100%', marginTop: -10 }}>
                  <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                    <Text adjustsFontSizeToFit={true} style={{ fontSize: RFPercentage(4), textAlign: 'center', }}>
                      {temperature}
                    </Text>
                    <Text style={{ fontSize: RFPercentage(3), }} >°</Text>
                  </View>
                </View>
              </View>
          }
        </View>
      </View>

    </View>
  )
}

const styles = StyleSheet.create({
  indicatorWarp: { flex: 1, justifyContent: 'center', alignItems: 'center', },

});